<?php

/**
 * Testing the Git Flow Workflow process.
 *
 * @version		0.3.1
 */
class TestGitFlow
{
	/**
	 * Contains a test variable. Can be an array or a string.
     *
     * @var			mixed
     * @access		private
     * @since		0.3
     * @updated		0.3.1
     * @default     null
	 */
	private $_x = null;
	
	/**
	 * Just echo something stringy for feature 1.
	 *
	 * @access		public
	 * @since		0.1
	 */
	public function thisIsATest()
	{
		echo "This is a test of the Git flow Workflow system";
	}
	
	/**
	 * Just echo something stringy for feature 2.
	 *
	 * @access		public
	 * @since		0.2
	 *
	 * @param		$x			array				A set of values.
	 */
	public function thisIsFeature2($x)
	{
		if (is_array($x)) {
			echo "This is an array";
		}
		else {
			echo "This is sparta!";
		}
	}
	
	/**
	 * Stores `$x` in the object and returns whether it
	 * succeeded or failed.
	 *
	 * @access		public
	 * @since		0.3
	 * @updated		0.3.1
	 * 
	 * @param		$x			mixed				Contains a set of values or a string with some text.
	 *
	 * @return		$tbr		boolean				True if this succeeds; false otherwise.
	 */
	public function thisIsFeature3($x)
	{
		$tbr = true;
		$this->_x = $x;
		return $tbr;
	}
}

$tgf = new TestGitFlow();

// Call the function that echos the text.
$tgf->thisIsATest();

// F2
$tgf->thisIsFeature2(array());
$tgf->thisIsFeature2(5);

// F3
if ($tgf->thisIsFeature3(5)) {
	echo "F3 succeeded";
}
?>